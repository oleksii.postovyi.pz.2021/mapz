﻿interface ICargoTransportable
{
    void Move(int distance, int cargoWeight);
    void Move(string city);
}

struct CargoTrain : ICargoTransportable
{
    private int vagons;
    private int weightCapacity;
    public CargoTrain() : this(0, 1) { }
    public CargoTrain(int capacity, int vagons)
    {
        this.weightCapacity = capacity;
        this.vagons = vagons;
    }
    public void Move(int distance, int cargoWeight)
    {
        Console.WriteLine($"Moved {cargoWeight} tons of cargo on {distance}km." +
            $"Average cargo weight per vagon: {Convert.ToDouble(cargoWeight / vagons)} tons" +
            $" Available weight left: {weightCapacity - cargoWeight} tons");
    }
    public void Move(string city)
    {
        Console.WriteLine($"Moving to {city}");
    }
}
