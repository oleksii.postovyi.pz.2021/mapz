﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_2._1
{
    interface ICargoTransportable
    {
        void Move(int distance, int cargoWeight);
    }

    abstract class CargoTransport : ICargoTransportable
    {
        protected int weightCapacity;
        public CargoTransport(int capacity)
        {
            this.weightCapacity = capacity;
        }
        public CargoTransport()
        {
            this.weightCapacity = 0;
        }

        public abstract void Move(int distance, int cargoWeight);
        public abstract void Move(string city);
    }

    class CargoTrain : CargoTransport, ICargoTransportable
    {
        private int vagons;
        public CargoTrain() : this(0, 1) { }
        public CargoTrain(int capacity, int vagons) : base(capacity)
        {
            this.vagons = vagons;
        }

        protected new object MemberwiseClone()
        {
            return base.MemberwiseClone();
        }
        public override bool Equals(object? obj)
        {
            return obj is CargoTrain train &&
                   weightCapacity == train.weightCapacity &&
                   vagons == train.vagons;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override string? ToString()
        {
            return $"Cargo train with {this.vagons} vagons. Can carry {this.weightCapacity} tons.";
        }
        
        public static new bool ReferenceEquals(object obj1, object obj2)
        {
            return obj1 == obj2;
        }
        public new Type GetType()
        {
            return base.GetType();
        }


        public override void Move(int distance, int cargoWeight)
        {
            Console.WriteLine($"Moved {cargoWeight} tons of cargo on {distance}km." +
                $"Average cargo weight per vagon: {Convert.ToDouble(cargoWeight / vagons)} tons" +
                $" Available weight left: {weightCapacity - cargoWeight} tons");
        }
        public override void Move(string city)
        {
            Console.WriteLine($"Moving to {city}");
        }

        
    }

}
