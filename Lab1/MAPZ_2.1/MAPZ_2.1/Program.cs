﻿// 2
AccessMods am1 = new AccessMods();
Console.WriteLine(am1.field1);
Console.WriteLine(am1.field2);
Console.WriteLine(am1.field3);
// 2 end

// 10
string name = "";

//boxing:
object o1 = name;

//unboxing:
string name2 = (string)o1;

// 10 end


// 1 & 6 & 7
interface ICargoTransportable
{
    void Move(int distance, int cargoWeight);
}

abstract class CargoTransport : ICargoTransportable
{
    protected int weightCapacity;
    public CargoTransport(int capacity)
    {
        this.weightCapacity = capacity;
    }
    public CargoTransport()
    {
        this.weightCapacity = 0;
    }

    public abstract void Move(int distance, int cargoWeight);
    public abstract void Move(string city);
}

class CargoTrain : CargoTransport, ICargoTransportable
{
    private int vagons;
    public CargoTrain() : this(0,1){}
    public CargoTrain(int capacity, int vagons) : base(capacity)
    {
        this.vagons = vagons;
    }
    public override void Move(int distance, int cargoWeight)
    {
        Console.WriteLine($"Moved {cargoWeight} tons of cargo on {distance}km." +
            $"Average cargo weight per vagon: {Convert.ToDouble(cargoWeight/vagons)} tons" +
            $" Available weight left: {weightCapacity - cargoWeight} tons");
    }
    public override void Move(string city)
    {
        Console.WriteLine($"Moving to {city}");
    }
}
// 1 & 6 & 7 end


// 2
class AccessMods
{
    public int field1;
    protected int field2;
    private int field3;
    public AccessMods()
    {

    }
    void TestAccess()
    {
        Console.WriteLine(field1);
        Console.WriteLine(field2);
        Console.WriteLine(field3);
    }
}

class ChildAccessMods : AccessMods
{
    void TestAccess()
    {
        Console.WriteLine(field1);
        Console.WriteLine(field2);
        Console.WriteLine(field3);
    }
}
// 2 end

// 3
class DefaultMods
{
    int defaultModField;
    public DefaultMods()
    {
        defaultModField = 0;
    }
    void DefaultModMethod()
    {
        Console.WriteLine("Default");
    }
    class DefaultInner
    {
        int defaultInnerField;
    }
}

struct DefaultStruct
{
    int defaultStructField;
}
interface IDefault
{
    void DefaultMethod();
}

// 3 end

// 4
class Outer
{
    private class PrivateInner
    {

    }
    protected class ProtectedInner
    {

    }
    PrivateInner pi1;
    ProtectedInner pi2;
}

class ChildOuter : Outer
{
    PrivateInner pi3;



    ProtectedInner pi4;
}
// 4 end

// 5
enum Months
{
    Jan = 1,
    Feb = 2,
    Mar = 4,
    Apr = 8,
    May = 16,
    Jun = 32,
    Jul = 64,
    Aug = 128,
    Sep = 256,
    Oct = 512,
    Nov = 1024,
    Dec = 2048,
}
class MonthsDemonstration
{
    public static void Demonstrate()
    {
        Months winter = Months.Dec | Months.Jan | Months.Feb;
        Months birthMonths = Months.Jan | Months.Mar | Months.Nov;
        Months birthdaysInWinter = birthMonths & winter;
        if(birthdaysInWinter != 0 && birthdaysInWinter == Months.Jan)
        {
            Console.WriteLine("They all have birthday in January");
        }
        Months spring = Months.Mar | Months.Apr | Months.May;
        Months birthdaysInSpring = birthMonths & spring;
        if((birthdaysInSpring != 0 || birthdaysInWinter != 0) && birthdaysInWinter != Months.Dec)
        {
            Console.WriteLine("They have birthdays in the first half of the year");
        }
        Months birthdaysNeeded = birthdaysInWinter ^ birthMonths;
    } 
}
// 5 end

// 8
class Car
{
    static int carCount = 0;
    int maxSpeed;
    static Car()
    {
        carCount++;
    }
    public Car(int maxSpeed)
    {
        this.maxSpeed = maxSpeed;
    }
}
// 8 end

// 9
class TempMath
{
    public void RectangleSquare(double d1, double d2, out double square) => square = 0.5 * d1 * d2;
    // square = 0.5 * d1 * d2
    public void TriangleSquare(double a, double h, ref double square) => square = 0.5 * a * h;
    // square = 0.5 * a * h

    double square;
    public void RectangleSquare2(double d1, double d2, double square) => square = 0.5 * d1 * d2;

    public double RectangleSquare3(double d1, double d2, ref double square) => 0.5 * d1 * d2;
    public double RectangleSquare4(double d1, double d2, double square) => 0.5 * d1 * d2;
   

}
// 9 end

// 11
class ExIm
{
    public int valProperty { get; set; }
    public static implicit operator ExIm(int x) { return new ExIm { valProperty = x }; }
    public static explicit operator int(ExIm x) { return x.valProperty;  }
}
// 11 end

// 14
