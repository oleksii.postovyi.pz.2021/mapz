﻿using System.Diagnostics;

Stopwatch stopwatch = new Stopwatch();
// 2 fields
Console.WriteLine("Testing struct with 2 fields");
fields2 f2 = new fields2();

// boxing
stopwatch.Start();
object o2 = f2;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

// unboxing
stopwatch.Start();
fields2 f22 = (fields2)o2;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

/*
// 20 fields
Console.WriteLine("Testing struct with 20 fields");
fields20 f20 = new fields20();

// boxing
stopwatch.Start();
object o20 = f20;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

// unboxing
stopwatch.Start();
fields20 f202 = (fields20)o20;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();


// 50 fields
Console.WriteLine("Testing struct with 50 fields");
fields50 f50 = new fields50();

// boxing
stopwatch.Start();
object o50 = f50;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

// unboxing
stopwatch.Start();
fields50 f502 = (fields50)o50;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

*/


struct fields2
{
    int field1;
    int field2;
}

struct fields20
{
    int field1;
    int field2;
    int field3;
    int field4;
    int field5;
    int field6;
    int field7;
    int field8;
    int field9;
    int field10;
    int field11;
    int field12;
    int field13;
    int field14;
    int field15;
    int field16;
    int field17;
    int field18;
    int field19;
    int field20;
}

struct fields50
{
    int field1;
    int field2;
    int field3;
    int field4;
    int field5;
    int field6;
    int field7;
    int field8;
    int field9;
    int field10;
    int field11;
    int field12;
    int field13;
    int field14;
    int field15;
    int field16;
    int field17;
    int field18;
    int field19;
    int field20;
    int field21;
    int field22;
    int field23;
    int field24;
    int field25;
    int field26;
    int field27;
    int field28;
    int field29;
    int field30;
    int field31;
    int field32;
    int field33;
    int field34;
    int field35;
    int field36;
    int field37;
    int field38;
    int field39;
    int field40;
    int field41;
    int field42;
    int field43;
    int field44;
    int field45;
    int field46;
    int field47;
    int field48;
    int field49;
    int field50;
}