﻿using System.Diagnostics;
Stopwatch stopwatch = new Stopwatch();
/*// int
Console.WriteLine("Testing for INT");
int a = 10;

// boxing
stopwatch.Start();
object o = a;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// unboxing
stopwatch.Start();
int b = (int)o;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// присвоєння
stopwatch.Start();
a = 134243564;
a = 500;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();

// double
Console.WriteLine("Testing for DOUBLE");
double a2 = 61.3254;

// boxing
stopwatch.Start();
object o2 = a2;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// unboxing
stopwatch.Start();
double b2 = (double)o2;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// присвоєння
stopwatch.Start();
a2 = 500.8841;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();
*/
// float
Console.WriteLine("Testing for FLOAT");
float a3 = 51.9f;

// boxing
stopwatch.Start();
object o3 = a3;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// unboxing
stopwatch.Start();
float b3 = (float)o3;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");

stopwatch.Reset();

// присвоєння
stopwatch.Start();
a3 = 83.23f;
stopwatch.Stop();
Console.WriteLine($"Elapsed ticks: {stopwatch.ElapsedTicks}");
stopwatch.Reset();